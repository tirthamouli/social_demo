const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const graphql = require('express-graphql');
const schema = require('./src/graphql/schema/index');

// Step 1: Create a new app
const app = express();

// Step 2: Use body-parser
app.use(bodyParser.json());

// Step 3: Define middleware
app.use('/graphql', graphql({
  schema,
  rootValue: {},
  graphiql: true,
}));

// Step 4: Connect to mongodb and listen to port
mongoose.connect(
  process.env.DB_URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  },
).then(() => {
  // Step 5.1: Listen on port
  app.listen(3000, () => {
  // eslint-disable-next-line no-console
    console.log('Listening to port 3000');
  });
}).catch((_err) => {
  // eslint-disable-next-line no-console
  console.log('Unable to connect to database');
});
