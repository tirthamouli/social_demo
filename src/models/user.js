/**
 * Schema and model for user
 * Author: Tirthamouli Baidya
 */

const { Schema, model } = require('mongoose');

const userSchema = new Schema({
  /**
   * Name of the user
   */
  name: {
    type: {
      /**
       * First name of the user
       */
      firstName: {
        type: String,
        required: true,
      },

      /**
       * Last name of the user
       */
      lastName: {
        type: String,
        required: true,
      },
    },
    required: true,
  },
  /**
   * Email of the user
   */
  email: {
    type: String,
    required: true,
  },
  /**
   * Password of the user
   */
  password: {
    type: String,
    required: true,
  },
});

userSchema.index({ email: 1 }, { unique: true });
module.exports = model('user', userSchema);
