/**
 * Post schemas and model
 * Author: Tirthamouli Baidya
 */

const { Schema, model } = require('mongoose');

const postSchema = new Schema({
  /**
   * Type of post
   */
  type: {
    type: String,
    required: true,
  },
  value: {
    type: String,
    require: true,
  },
  /**
   * Description of the post
   */
  description: {
    type: String,
    required: true,
  },
  /**
   * Timestamp at which the post is created
   */
  createdAt: {
    type: Number,
    require: true,
  },
  /**
   * The creator of the event
   */
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true,
  },
});

module.exports = model('post', postSchema);
