/**
 * Comment schemas and model
 * Author: Tirthamouli Baidya
 */

const { Schema, model } = require('mongoose');

const commentSchema = new Schema({
  /**
   * Comment value
   */
  value: {
    type: String,
    require: true,
  },
  /**
   * Timestamp at which the post is created
   */
  createdAt: {
    type: Number,
    require: true,
  },
  /**
   * The creator of the event
   */
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true,
  },
  /**
   * The post for which the comment is added
   */
  post: {
    type: Schema.Types.ObjectId,
    ref: 'post',
    required: true,
  },
});

module.exports = model('post', commentSchema);
