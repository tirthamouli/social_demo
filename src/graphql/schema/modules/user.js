/**
 * User Schema: Schema and input related to users
 * Author: Tirthamouli Baidya
 */

/**
 * The user schema
 */
exports.User = `type User {
  id: ID!
  firstName: String!
  lastName: String!
  email: String!
  password: String
  posts: [Post!]!
}`;

/**
 * The auth token schema
 */
exports.Auth = `type {
  firstName: String!
  lastName: String!
  email: String!
  token: String!
}`;

/**
 * User input for login
 */
exports.LoginInput = `input LoginInput {
  email: String!
  password: String
}`;

/**
 * User input for register
 */
exports.ResigterInput = `input RegisterInput {
  firstName: String!
  lastName: String!
  email: String!
  password: String
}`;

/**
 * User queries
 */
exports.userQueries = `
  user(commentPage: Number): User
`;

/**
 * User mutation
 */
exports.userMutations = `
  register(registerInput: RegisterInput): Auth
  login(loginInput: LoginInput): Auth
`;
