/**
 * Comment schema - Schema and input related to post
 * Author: Tirthamouli Baidya
 */

/**
 * The comment schema
 */
exports.Comment = `type Post {
  id: ID!
  value: String!
  createdAt: String!
  createdBy: User!
  post: Post
}`;

/**
 * Comment input
 */
exports.CommentInput = `input CommentInput {
  value: String!
  postID: ID!
}`;

/**
 * Comment mutations
 */
exports.CommentMuation = `
  createComment(commentInput: CommentInput): Comment
`;
