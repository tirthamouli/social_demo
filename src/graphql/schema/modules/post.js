/**
 * Post Schema: Schema and input related to posts
 * Author: Tirthamouli Baidya
 */

/**
 * The post schema
 */
exports.Post = `type Post {
  id: ID!
  type: String!
  value: String!
  description: String
  createdAt: String!
  createdBy: User!
}`;

/**
 * Add post input
 */
exports.PostInput = `input PostInput {
  type: String!
  value: String!
  description: String
}`;

/**
 * Post queries
 */
exports.PostQuery = `
  posts:(page: Number): [Post!]!
`;

/**
 * Post mutation
 */
exports.PostMutation = `
  createPost(postInput: PostInput): Post
`;
