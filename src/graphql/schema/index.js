/**
 * Combining all the schemas
 * AUTHOR: Tirthamouli Baidya
 */

const { buildSchema } = require('graphql');

// Schema, queries and mutations import
const {
  User, Auth, LoginInput, ResigterInput, userQueries, userMutations,
} = require('./modules/user');
const {
  Post, PostInput, PostQuery, PostMutation,
} = require('./modules/post');
const { Comment, CommentInput, CommentMuation } = require('./modules/comment');

// Building the schema
export default buildSchema(`
    ${User}
    ${Auth}
    ${LoginInput}
    ${ResigterInput}
    ${Post}
    ${PostInput}
    ${Comment}
    ${CommentInput}
    
    type RootQuery {
      ${userQueries}
      ${PostQuery}
    }

    type RootMutation {
      ${userMutations}
      ${PostMutation}
      ${CommentMuation}
    }
  
    schema {
      query: RootQuery
      mutation: RootMutation   
    }
  `);
